package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public final class NullProjectException extends AbstractException {

    public NullProjectException() {
        super("Error! ProjectGraph is null...");
    }

}
