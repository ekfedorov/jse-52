package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.*;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.api.service.dto.*;
import ru.ekfedorov.tm.api.service.model.*;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.service.dto.*;
import ru.ekfedorov.tm.service.model.*;
import ru.ekfedorov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    public final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    public final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    public final IProjectService projectDTOService = new ProjectService(connectionService);

    @NotNull
    public final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    public final IProjectGraphService projectService = new ProjectGraphService(connectionService);

    @NotNull
    public final IProjectTaskService projectTaskDTOService = new ProjectTaskService(connectionService);

    @NotNull
    public final IProjectTaskGraphService projectTaskService = new ProjectTaskGraphService(connectionService);

    @NotNull
    public final ISessionService sessionDTOService = new SessionService(connectionService, this);

    @NotNull
    public final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    public final ISessionGraphService sessionService = new SessionGraphService(connectionService, this);

    @NotNull
    public final ITaskService taskDTOService = new TaskService(connectionService);

    @NotNull
    public final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    public final ITaskGraphService taskService = new TaskGraphService(connectionService);

    @NotNull
    public final IUserService userDTOService = new UserService(propertyService, connectionService);

    @NotNull
    public final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    public final IUserGraphService userService = new UserGraphService(propertyService, connectionService);

    @NotNull
    public IActiveMQConnectionService activeMQConnectionService;

    @NotNull
    public final ExecutorService executorService = new ExecutorService();

    @Nullable
    private Session session = null;

    @SneakyThrows
    public void init() {
        initPID();
        initJMSBroker();
        initActiveMQConnectionService();
        initEndpoint();
        initUser();
    }

    private void initActiveMQConnectionService() {
        activeMQConnectionService = new ActiveMQConnectionService(this);
    }

    @SneakyThrows
    public void initJMSBroker() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":" + BrokerService.DEFAULT_PORT;
        broker.addConnector(bindAddress);
        broker.start();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(adminEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initUser() {
        if (!userService.findByLogin("test").isPresent()) {
            userService.create("test", "test", "test@test.ru");
        }
        if (!userService.findByLogin("test2").isPresent()) {
            userService.create("test2", "test", "test@test.ru");
        }
        if (!userService.findByLogin("admin").isPresent()) {
            userService.create("admin", "admin", Role.ADMIN);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
