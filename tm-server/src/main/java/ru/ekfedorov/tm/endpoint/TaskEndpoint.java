package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.ITaskEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.dto.Task;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskDTOService().add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectTaskDTOService().bindTaskByProject(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectTaskDTOService().findAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskDTOService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @NotNull
    public List<Task> findTaskAllWithComparator(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskDTOService().findAll(session.getUserId(), sort);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskDTOService().findOneById(session.getUserId(), id).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskDTOService().findOneByIndex(session.getUserId(), index).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskDTOService().findOneByName(session.getUserId(), name).orElse(null);
    }

    @Override
    @WebMethod
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().remove(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getProjectTaskDTOService().unbindTaskFromProject(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionDTOService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getTaskDTOService().updateByIndex(session.getUserId(), index, name, description);
    }

}
