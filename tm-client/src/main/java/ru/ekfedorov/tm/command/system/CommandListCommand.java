package ru.ekfedorov.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import java.util.Collection;

public final class CommandListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show program commands.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "commands";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final Collection<String> commands = serviceLocator.getCommandService().getListCommandName();
        System.out.println("[COMMANDS]");
        for (@Nullable final String command : commands) {
            if (command != null) System.out.println(command);
        }
    }

}
