package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getArgsCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getListArgumentName();

    @NotNull
    Collection<String> getListCommandName();

}
